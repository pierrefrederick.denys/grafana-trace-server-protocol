import defaults from 'lodash/defaults';

import {
  DataQueryRequest,
  DataQueryResponse,
  DataSourceApi,
  DataSourceInstanceSettings,
  MutableDataFrame,
  FieldType,
} from '@grafana/data';

import { getBackendSrv } from '@grafana/runtime';

import { MyQuery, MyDataSourceOptions, defaultQuery } from './types';

export class DataSource extends DataSourceApi<MyQuery, MyDataSourceOptions> {
  tsHostname: string | undefined;

  constructor(instanceSettings: DataSourceInstanceSettings<MyDataSourceOptions>) {
    super(instanceSettings);
    this.tsHostname = instanceSettings.jsonData.tsHostname;
  }

  //query.fields[0].jsonPath +, to get filter parameter
  // do a request on the trace server
  async doRequest(query: MyQuery, type: string, payload: string) {
    let prefix = '';
    if (query.dataModel === 'XY') {
      prefix = '/xy';
    } else if (query.dataModel === 'table') {
      if (type === 'first') {
        prefix = '/columns';
      } else if (type === 'lines') {
        prefix = '/lines';
      }
    } else if (query.dataModel === 'timeGraph') {
      if (type === 'first') {
        prefix = '/tree';
      } else if (type === 'states') {
        prefix = '/states';
      } else if (type === 'tooltip') {
        prefix = '/tooltip';
      }
    }
    const result = await getBackendSrv().datasourceRequest({
      method: 'POST',
      url:
        this.tsHostname +
        '/tsp/api/experiments/' +
        query.expUUID +
        '/outputs/' +
        query.dataModel +
        '/' +
        query.outputProvider +
        prefix,
      data: payload,
    });
    return result;
  }

  async query(options: DataQueryRequest<MyQuery>): Promise<DataQueryResponse> {
    let { range } = options;
    let from = range!.from.valueOf();
    console.log(from);
    let to = range!.to.valueOf();
    console.log(to);
    const type = 'first';
    let current = from * 1000000;
    let timeArray: number[] = [];
    while (current <= to * 1000000) {
      current = current + 19465472;
      timeArray.push(current);
    }
    let payload = '{"parameters":{"requested_times":[' + timeArray + '],"requested_items":[]}},';
    const promises = options.targets.map(target =>
      this.doRequest(target, type, payload).then(async response => {
        const query = defaults(target, defaultQuery);
        //console.log(response);
        let frame = new MutableDataFrame();
        if (query.dataModel === 'XY') {
          frame = new MutableDataFrame({
            refId: query.refId,
            fields: [
              { name: 'Time', type: FieldType.time },
              { name: 'Value', type: FieldType.number },
              { name: 'Value2', type: FieldType.number },
            ],
          });
          //console.log(response.data.model.series['0'].xValues);
          var xarray = response.data.model.series['0'].xValues;
          var yarray = response.data.model.series['0'].yValues;
          xarray.forEach((time: any, i: any) => {
            frame.appendRow([Math.floor(time / 1000000), yarray[i], yarray[i]]);
          });
        } else if (query.dataModel === 'table') {
          frame = new MutableDataFrame({
            refId: query.refId,
            fields: [],
          });
          let columnsSelected: any[] = [];

          response.data.model.forEach((objec: any, i: any) => {
            frame.addField({ name: objec['name'], type: FieldType.string });
            columnsSelected.push(i);
          });

          var payload = '{"parameters":{"lowIndex":0,"size":200,"columnIds":[' + columnsSelected + ']}}';

          const result = await getBackendSrv().datasourceRequest({
            method: 'POST',
            url:
              this.tsHostname +
              '/tsp/api/experiments/' +
              query.expUUID +
              '/outputs/' +
              query.dataModel +
              '/' +
              query.outputProvider +
              '/lines',
            data: payload,
          });

          result.data.model.lines.forEach((line: any, i: any) => {
            let arrayline: any[] = [];
            line.cells.forEach((cell: any, i: any) => {
              arrayline.push(cell.content);
            });
            frame.appendRow(arrayline);
            //console.log(arrayline);
          });
        } else if (query.dataModel === 'timeGraph') {
          frame = new MutableDataFrame({
            refId: query.refId,
            fields: [
              { name: 'Time', type: FieldType.time },
              { name: 'Nano', type: FieldType.number },
              { name: 'Duration', type: FieldType.number },
              { name: 'Name', type: FieldType.string },
              { name: 'Target', type: FieldType.string },
            ],
          });
          let columnsSelected: any[] = [];
          let columnsNames: any[] = [];
          console.log(response.data.model.entries);
          response.data.model.entries.forEach((objec: any, i: any) => {
            columnsSelected.push(objec['id']);
            columnsNames.push('a' + objec['id'] + objec['labels'][0] + '');
          });

          var payload2 =
            '{"parameters":{"requested_times":[' + timeArray + '],"requested_items":[' + columnsSelected + ']}}';

          const result = await getBackendSrv().datasourceRequest({
            method: 'POST',
            url:
              this.tsHostname +
              '/tsp/api/experiments/' +
              query.expUUID +
              '/outputs/' +
              query.dataModel +
              '/' +
              query.outputProvider +
              '/states',
            data: payload2,
          });
          result.data.model.rows.forEach((line: any, i: any) => {
            line.states.forEach((state: any, j: any) => {
              if (state.label != null) {
                frame.appendRow([
                  Math.floor(state.start / 1000000),
                  state.start,
                  state.end - state.start,
                  state.label,
                  columnsNames[i],
                ]);
              }
            });
          });
        }
        console.log('This is the frame :');
        console.log(frame);
        return frame;
      })
    );

    return Promise.all(promises).then(data => ({ data }));
  }

  async test() {
    const req = {
      url: 'https://api.example.com/metrics',
      method: 'GET',
    };
    return getBackendSrv().datasourceRequest(req);
  }

  // health check of the trace server
  async testDatasource() {
    const defaultMsg = 'Cannot connect to Trace Server';

    try {
      const response = await this.test();

      if (response.status === 200) {
        return {
          status: 'success',
          message: 'Success',
        };
      } else {
        return {
          status: 'error',
          message: response.statusText ? response.statusText : defaultMsg,
        };
      }
    } catch (err) {
      let message = 'Response : ';
      message += err.statusText ? err.statusText : defaultMsg;
      if (err.data && err.data.error && err.data.error.code) {
        message += ': ' + err.data.error.code + '. ' + err.data.error.message;
      }

      return {
        status: 'Error ',
        message,
      };
    }
  }
}
