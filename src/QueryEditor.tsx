import defaults from 'lodash/defaults';

import React, { ChangeEvent, PureComponent } from 'react';
import { Icon, InlineFormLabel, LegacyForms } from '@grafana/ui';
import { QueryEditorProps } from '@grafana/data';
import { DataSource } from './DataSource';
import { defaultQuery, MyDataSourceOptions, MyQuery } from './types';
import { JsonPathQueryField } from './JsonPathQueryField';
import { cx } from 'emotion';
const { FormField } = LegacyForms;

type Props = QueryEditorProps<DataSource, MyQuery, MyDataSourceOptions>;

export class QueryEditor extends PureComponent<Props> {
  onExpUUIDChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, expUUID: event.target.value });
  };

  ondataModelChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, dataModel: event.target.value });
  };

  onOutputProviderChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, outputProvider: event.target.value });
  };

  /*onConstantChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query, onRunQuery } = this.props;
    onChange({ ...query, constant: parseFloat(event.target.value) });
    // executes the query
    onRunQuery();
  };*/

  onFieldNameChange = (i: number) => (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query, onRunQuery } = this.props;
    const { fields } = defaults(query, defaultQuery);
    fields[i] = { ...fields[i], name: event.target.value };
    onChange({ ...query, fields });
    onRunQuery();
  };

  onChangePath = (i: number) => (e: string) => {
    const { onChange, query, onRunQuery } = this.props;
    const { fields } = defaults(query, defaultQuery);
    fields[i] = { ...fields[i], jsonPath: e };
    onChange({ ...query, fields });
    onRunQuery();
  };

  addField = (i: number) => () => {
    const { onChange, query } = this.props;
    const { fields } = defaults(query, defaultQuery);
    if (fields) {
      fields.splice(i + 1, 0, { name: '', jsonPath: '' });
    }
    onChange({ ...query, fields });
  };

  removeField = (i: number) => () => {
    const { onChange, query, onRunQuery } = this.props;
    const { fields } = defaults(query, defaultQuery);
    if (fields) {
      fields.splice(i, 1);
    }
    onChange({ ...query, fields });
    onRunQuery();
  };

  render() {
    const { fields } = defaults(this.props.query, defaultQuery);
    const { expUUID: expUUID, dataModel: dataModel, outputProvider: outputProvider } = defaults(
      this.props.query,
      defaultQuery
    );
    const { onRunQuery } = this.props;

    return (
      <>
        <div className="gf-form">
          <FormField
            labelWidth={12}
            inputWidth={200}
            value={expUUID || ''}
            onChange={this.onExpUUIDChange}
            label="UUID of experiment"
            tooltip="UUID of the experiment to query"
          />
        </div>
        <div className="gf-form">
          <FormField
            labelWidth={12}
            inputWidth={50}
            value={dataModel || ''}
            onChange={this.ondataModelChange}
            label="Model of Data"
            tooltip="xy, timegraph"
          />
        </div>
        <div className="gf-form">
          <FormField
            labelWidth={12}
            inputWidth={50}
            value={outputProvider || ''}
            onChange={this.onOutputProviderChange}
            label="Output provider"
            tooltip="ID of the output provider"
          />
        </div>
        {fields
          ? fields.map((field, index) => (
              <div key={index} className="gf-form">
                <InlineFormLabel
                  width={7}
                  className="query-keyword"
                  tooltip={
                    <p>
                      A <a href="https://goessner.net/articles/JsonPath/">JSON Path</a> query that selects one or more
                      values from a JSON object.
                    </p>
                  }
                >
                  Filter
                </InlineFormLabel>
                <JsonPathQueryField onBlur={onRunQuery} onChange={this.onChangePath(index)} query={field.jsonPath} />
                <InlineFormLabel width={3} className="query-keyword">
                  Alias
                </InlineFormLabel>
                <input
                  className="gf-form-input width-14"
                  value={field.name || ''}
                  onChange={this.onFieldNameChange(index)}
                ></input>

                <a className={cx('gf-form-label', 'gf-form-label--grow')} onClick={this.addField(index)}>
                  <Icon name="plus" />
                </a>
                {fields.length > 1 ? (
                  <a className={cx('gf-form-label', 'gf-form-label--grow')} onClick={this.removeField(index)}>
                    <Icon name="minus" />
                  </a>
                ) : null}
              </div>
            ))
          : null}
      </>
    );
  }
}
