import { DataQuery, DataSourceJsonData } from '@grafana/data';

interface JsonField {
  name: string;
  jsonPath: string;
}

export interface MyQuery extends DataQuery {
  expUUID?: string;
  dataModel?: string;
  outputProvider?: string;
  constant: number;
  fields: JsonField[];
}

export interface JsonApiVariableQuery extends DataQuery {
  jsonPath: string;
}

export const defaultQuery: Partial<MyQuery> = {
  constant: 6.5,
  fields: [{ name: '', jsonPath: '' }],
};

/**
 * These are options configured for each DataSource instance
 */
export interface MyDataSourceOptions extends DataSourceJsonData {
  path?: string;
  tsHostname?: string;
}

/**
 * Value that is used in the backend, but never sent over HTTP to the frontend
 */
export interface MySecureJsonData {
  apiKey?: string;
}
