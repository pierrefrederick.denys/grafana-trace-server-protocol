

# Trace server protocol Plugin for Grafana

04/01/2020 : This plugin is under active development.

This plugin is tested for Grafana 7.0+.

It is based on [Grafana Datasource Plugin Template](https://github.com/grafana/grafana-starter-datasource) and  [JSON Datasource Plugin](https://github.com/marcusolsson/grafana-json-datasource)

## What is Grafana Trace server protocol Plugin?
A datasource plugin is used to get data from a data source like a database or a REST API. 
This plugin allows to read a trace opened on a trace server with the [Trace Server Protocol](https://theia-ide.github.io/trace-server-protocol/)

Read the [Full documentation](https://synedtech.com/fr/tracing/1/Grafana-data-provider-plugin-for-Trace-server-protocol-and-LTTng)

## Purpose
For the moment, this module is intended to be used only with a Trace server using the [Trace Server Protocol](https://theia-ide.github.io/trace-server-protocol/).
A trace generated with  [LTTng](https://lttng.org/) is uploaded on a trace server, and the data source plugin make query on this server to exploit trace data.

## Usage

This plugin is in development and is NOT yet intended for production usage. You can test it by following these steps :

1 . Locate and go in your plugin directory of grafana server (`grafana-plugins`)
2. Clone this repository inside of plugin directory
3.  Install dependencies
```BASH
yarn install
```
4. Build plugin in production mode
```BASH
yarn build
```
5. Restart the grafana server daemon or restart docker container
6. Go to `http://<grafana-instance-url>:<port>/datasources/new`  You should see a new datasource type called `trace-server-protocol`in "others" at the bottom of the page.
 

## Development guidelines

There is no official documentation for this plugin, but here are some information about the implementation :

The main logic is located in **DataSource.ts** file.

Read more in the [Full documentation](https://synedtech.com/fr/tracing/1/Grafana-data-provider-plugin-for-Trace-server-protocol-and-LTTng)



## Learn more
- [Build a data source plugin tutorial](https://grafana.com/tutorials/build-a-data-source-plugin)
- [Grafana documentation](https://grafana.com/docs/)
- [Grafana Tutorials](https://grafana.com/tutorials/) - Grafana Tutorials are step-by-step guides that help you make the most of Grafana
- [Grafana UI Library](https://developers.grafana.com/ui) - UI components to help you build interfaces using Grafana Design System
